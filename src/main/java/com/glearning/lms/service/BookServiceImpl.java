package com.glearning.lms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.glearning.lms.model.Book;
import com.glearning.lms.repository.BookJpaRepository;


//Singleton bean
@Service
public class BookServiceImpl implements BookService {
	
	//constructor based dependency injection
	private final BookJpaRepository bookRepository;
	
	public BookServiceImpl(BookJpaRepository repository) {
		this.bookRepository = repository;
	}

	@Override
	public List<Book> findAll() {
		return this.bookRepository.findAll();
	}

	@Override
	public Book findBookById(int bookId) {
		return this.bookRepository.findById(bookId).orElseThrow(() -> new IllegalArgumentException("invalid book id"));
	}

	@Override
	public Book save(Book book) {
		return this.bookRepository.save(book);
	}

	@Override
	public void deleteBookById(int bookId) {
		this.bookRepository.deleteById(bookId);
	}

}
