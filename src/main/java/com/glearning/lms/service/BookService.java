package com.glearning.lms.service;

import java.util.List;

import com.glearning.lms.model.Book;

public interface BookService {
	
	List<Book> findAll();
	
	Book findBookById(int bookId);
	
	Book save(Book book);
	
	void deleteBookById(int bookId);

}
