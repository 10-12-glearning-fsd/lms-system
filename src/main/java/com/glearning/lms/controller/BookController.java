package com.glearning.lms.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.glearning.lms.model.Book;
import com.glearning.lms.service.BookService;

@Controller
@RequestMapping("/books")
public class BookController {
	
	private final BookService bookService;
	
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}
	
	@GetMapping("/list")
	public String listBooks(Model model) {
		List<Book> books = this.bookService.findAll();
		model.addAttribute("books", books);
		return "books/list-books";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {
		Book book = new Book();
		model.addAttribute("book", book);
		return "books/book-form";
	}
	
	@PostMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("bookId") int bookId, Model model) {
		Book bookFetchedFromDB = this.bookService.findBookById(bookId);
		// set book as a model attribute to pre-populate the form
		model.addAttribute("book", bookFetchedFromDB);
		// send over to our form
		return "books/book-form";
	}
	
	
	@PostMapping("/author")
	public String displayAuthoDetails(@RequestParam("bookId") int bookId, Model model) {
		Book bookFetchedFromDB = this.bookService.findBookById(bookId);
		// set book as a model attribute to pre-populate the form
		model.addAttribute("author", bookFetchedFromDB.getAuthor());
		// send over to our form
		return "books/author-details";
	}
	
	
	@PostMapping("/save")
	public String saveBook(@ModelAttribute("book") Book book) {
		this.bookService.save(book);
		return "redirect:/books/list";
	}
	
	@PostMapping("/delete")
	public String deleteBook(@RequestParam("bookId") int bookId) {
		this.bookService.deleteBookById(bookId);
		return "redirect:/books/list";
	}


}
