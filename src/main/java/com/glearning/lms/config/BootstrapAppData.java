package com.glearning.lms.config;

import java.time.LocalDate;
import java.util.stream.IntStream;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.github.javafaker.Faker;
import com.glearning.lms.model.Author;
import com.glearning.lms.model.Book;
import com.glearning.lms.repository.BookJpaRepository;

@Configuration
public class BootstrapAppData {
	
	private final BookJpaRepository bookRepository;
	private final Faker faker = new Faker();
	
	public BootstrapAppData(BookJpaRepository bookRepository) {
		this.bookRepository = bookRepository;
	}
	
	//this method will be called once the application is ready to accept the requests
	@EventListener(ApplicationReadyEvent.class)
	void onReadyEvent(ApplicationReadyEvent readyEvent) {
		IntStream.range(1,100).forEach(value -> {
			Author author = new Author(faker.name().firstName(), faker.internet().emailAddress(),
					LocalDate.now()
					);
			Book book = new Book(faker.book().title(), author, faker.book().genre());
			book.setAuthor(author);
			author.setBook(book);
			this.bookRepository.save(book);
		});
	}

}
